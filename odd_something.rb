class OddSomething
  attr_reader :input

  def initialize(input)
    @input = input
  end

  def execute
    return unless input.is_a?(Array)

    hash = Hash.new(0)
    input.each do |item|
      hash[item] = hash[item] + 1
    end
    hash.filter{ |k, v| v.odd? }.keys.first.to_s
  end

end
