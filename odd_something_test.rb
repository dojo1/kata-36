require_relative 'odd_something'
require 'test/unit'

class OddSomethingTest < Test::Unit::TestCase
  def test_case_0
    input = [7]
    assert_equal "7", OddSomething.new(input).execute
  end

  def test_case_1
    input = [0]
    assert_equal "0", OddSomething.new(input).execute
  end

  def test_case_2
    input = [1,1,2]
    assert_equal "2", OddSomething.new(input).execute
  end

  def test_case_3
    input = [1,2,2,3,3,3,4,3,3,3,2,2,1]
    assert_equal "4", OddSomething.new(input).execute
  end

end
